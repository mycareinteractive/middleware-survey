package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sf_response database table.
 * 
 */
@Entity
@Table(name="sf_response")
@NamedQuery(name="SfResponse.findAll", query="SELECT s FROM SfResponse s")
public class SfResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to SfQuestionAnswer
	@OneToMany(mappedBy="sfResponse")
	private List<SfQuestionAnswer> sfQuestionAnswers;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="text")
	private TransTranslation transTranslation;

	public SfResponse() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<SfQuestionAnswer> getSfQuestionAnswers() {
		return this.sfQuestionAnswers;
	}

	public void setSfQuestionAnswers(List<SfQuestionAnswer> sfQuestionAnswers) {
		this.sfQuestionAnswers = sfQuestionAnswers;
	}

	public SfQuestionAnswer addSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		getSfQuestionAnswers().add(sfQuestionAnswer);
		sfQuestionAnswer.setSfResponse(this);

		return sfQuestionAnswer;
	}

	public SfQuestionAnswer removeSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		getSfQuestionAnswers().remove(sfQuestionAnswer);
		sfQuestionAnswer.setSfResponse(null);

		return sfQuestionAnswer;
	}

	public TransTranslation getTransTranslation() {
		return this.transTranslation;
	}

	public void setTransTranslation(TransTranslation transTranslation) {
		this.transTranslation = transTranslation;
	}

}