package com.aceso.kaiser.survey.service;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aceso.kaiser.survey.model.Dependent;
import com.aceso.kaiser.survey.model.SfQuestion;
import com.aceso.kaiser.survey.model.SfSurvey;


@Service("dependentService")
public class DependentService {
	
	@Autowired
	private DependentRepository dependentDAO; 
	
	protected static Logger log = LoggerFactory.getLogger("SurveyService");
	
	public DependentService() {
		log.debug("DependentService Constructor executed succesfully");
	}
	
	public Dependents getDependentDetails(String patientMRN) {
		List<Dependent> dependents = dependentDAO.getDependentDetails(patientMRN);
		
		if(dependents != null)
			log.info("Dependent Size"+dependents.size());
		DependentType dependentType = null;
		List<DependentType> dependentList = new ArrayList<DependentType>();
		  
		for(Dependent dependent:dependents)
		{
			    log.info("Dependent "+dependent);
		    	dependentType= new DependentType(dependent.getId(),dependent.getPatientMRN(),dependent.getDependentMRN(),dependent.getRelationship());
		    	dependentList.add(dependentType);
		    	dependentType=null;
		}
		Dependents dependent= new Dependents();
		dependent.setDependents(dependentList);
		return dependent;
	}




}
