package com.aceso.kaiser.survey.service;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import java.util.List;


public class SfQuestionType implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;

	private String answertype;

	private String displaycondition;

	private int sequence;
    private String title;
    private String text;
    private List<SfAnswerType> answer;

	public SfQuestionType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnswertype() {
		return this.answertype;
	}

	public void setAnswertype(String answertype) {
		this.answertype = answertype;
	}

	public String getDisplaycondition() {
		return this.displaycondition;
	}

	public void setDisplaycondition(String displaycondition) {
		this.displaycondition = displaycondition;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<SfAnswerType> getAnswer() {
		return answer;
	}
	
	@XmlElementWrapper(name="answers")
	@XmlElement(name="answer")
	public void setAnswer(List<SfAnswerType> answer) {
		this.answer = answer;
	}

	
	

}