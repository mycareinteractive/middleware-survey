package com.aceso.kaiser.survey.service;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


public class SfAnswerType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;

	private String value;

	private String text;
	
	private int sequence;

	private SfResponseType response;

	public SfAnswerType() {
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public SfResponseType getResponse() {
		return response;
	}

	public void setResponse(SfResponseType sfResponse) {
		this.response = sfResponse;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}




}