package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the trans_translation_entry database table.
 * 
 */
@Embeddable
public class TransTranslationEntryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int id;

	@Column(name="language_code")
	private String languageCode;

	@Column(name="translation_id", insertable=false, updatable=false)
	private int translationId;

	public TransTranslationEntryPK() {
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLanguageCode() {
		return this.languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public int getTranslationId() {
		return this.translationId;
	}
	public void setTranslationId(int translationId) {
		this.translationId = translationId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TransTranslationEntryPK)) {
			return false;
		}
		TransTranslationEntryPK castOther = (TransTranslationEntryPK)other;
		return 
			(this.id == castOther.id)
			&& this.languageCode.equals(castOther.languageCode)
			&& (this.translationId == castOther.translationId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.id;
		hash = hash * prime + this.languageCode.hashCode();
		hash = hash * prime + this.translationId;
		
		return hash;
	}
}