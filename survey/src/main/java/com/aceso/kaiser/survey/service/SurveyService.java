package com.aceso.kaiser.survey.service;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aceso.kaiser.survey.model.SfQuestion;
import com.aceso.kaiser.survey.model.SfQuestionAnswer;
import com.aceso.kaiser.survey.model.SfQuestionAnswerNotification;
import com.aceso.kaiser.survey.model.SfSurvey;
import com.aceso.kaiser.survey.model.TransTranslationEntry;

@Service("surveyService")
public class SurveyService {
	
	@Autowired
	private SurveyRepository surveyDao; 
	
	protected static Logger log = LoggerFactory.getLogger("SurveyService");
	
	public SurveyService() {
		log.debug("SurveyService Constructor executed succesfully");
	}
	
	public SfSurveyType generateSurveyXML(String surveyId,String languageName)  {
		  try{
		    String languageCode = surveyDao.getLanguageCode(languageName);
		  	    	
		    List<SfSurvey> surveyList = surveyDao.getSurveyDetails(surveyId);
		    SfSurveyType surveyType = null;
		    List<SfQuestionType> questionList = new ArrayList<SfQuestionType>();
		  
		    for(SfSurvey survey:surveyList)
		    {
		    	surveyType= new SfSurveyType(survey.getId(),getTheTranslation(survey.getTransTranslation2().getTransTranslationEntries(),survey.getTransTranslation2().getId(),languageCode),survey.getDisabled(),getTheTranslation(survey.getTransTranslation1().getTransTranslationEntries(),survey.getTransTranslation1().getId(),languageCode));
		    	
		    	for(SfQuestion sfQuestion:survey.getSfQuestions() )
		    	 {
		    		 
		    		  SfQuestionType sfQuestionType = new SfQuestionType();
		    		  sfQuestionType.setAnswertype(sfQuestion.getAnswertype());
		    		  sfQuestionType.setDisplaycondition(sfQuestion.getDisplaycondition());
		    		  sfQuestionType.setId(sfQuestion.getId());
		    		  sfQuestionType.setSequence(sfQuestion.getSequence());
		    		  sfQuestionType.setTitle(getTheTranslation(sfQuestion.getTransTranslation2().getTransTranslationEntries(),sfQuestion.getTransTranslation2().getId(),languageCode));
		    		  sfQuestionType.setText(getTheTranslation(sfQuestion.getTransTranslation1().getTransTranslationEntries(),sfQuestion.getTransTranslation1().getId(),languageCode));
		    		  
		    		  //Getting all the possible answers for that question
		    		  List<SfQuestionAnswer> sfQuestionAnswers = sfQuestion.getSfQuestionAnswers();
		    		  List<SfAnswerType> answerList = new ArrayList<SfAnswerType>();
		    		  for(SfQuestionAnswer questionanswer : sfQuestionAnswers)
		    		  {
		    			  SfAnswerType answerType = new SfAnswerType();
		    			  answerType.setId(questionanswer.getSfAnswer().getId());
		    			  answerType.setSequence(questionanswer.getSequence());
		    			  answerType.setText( getTheTranslation(questionanswer.getSfAnswer().getTransTranslation().getTransTranslationEntries(),questionanswer.getSfAnswer().getTransTranslation().getId(),languageCode));
		    			  answerType.setValue(questionanswer.getSfAnswer().getValue());
		    			  
		    			  //for each question and an answer combination, there is a  single response and a notification
		    			  SfResponseType sfResponse = new SfResponseType();
		    			  sfResponse.setId(questionanswer.getSfResponse().getId());
		    			  sfResponse.setText(getTheTranslation(questionanswer.getSfResponse().getTransTranslation().getTransTranslationEntries(),questionanswer.getSfResponse().getTransTranslation().getId(),languageCode));
						  
		    			  
		    			  List<SfNotificationType> notifications = new ArrayList<SfNotificationType>();
		    			  List<SfQuestionAnswerNotification> questionAnswerNotifications = questionanswer.getSfQuestionAnswerNotifications();
		    			  for(SfQuestionAnswerNotification qanotification : questionAnswerNotifications )
		    			  {
		    				  SfNotificationType sfNotification = new SfNotificationType();
		    				  sfNotification.setNotificationcondition(qanotification.getNotificationcondition());
		    				  sfNotification.setBody(qanotification.getSfNotification().getBody());
		    				  sfNotification.setDescription(qanotification.getSfNotification().getDescription());
		    				  sfNotification.setName(qanotification.getSfNotification().getName());
		    				  sfNotification.setParam(qanotification.getSfNotification().getParam());
		    				  sfNotification.setPayload(qanotification.getSfNotification().getPayload());
		    				  sfNotification.setSubject(qanotification.getSfNotification().getSubject());
		    				  sfNotification.setType(qanotification.getSfNotification().getType());
		    				  sfNotification.setUri(qanotification.getSfNotification().getUri());
		    				  
		    				  //Adding notifications to the response
		    				  notifications.add(sfNotification);
		    				  sfResponse.setNotification(notifications);
		    				
		    			  }
		    			  answerType.setResponse(sfResponse );
						  
		    			  answerList.add(answerType);
		    		  }
		    		  sfQuestionType.setAnswer(answerList);
		    		  questionList.add(sfQuestionType);
		   	  	  }
		     
		     surveyType.setQuestions(questionList);
		    }
		    
			return surveyType;
		  }catch(Exception e)
		  {
			  log.info("Info Error occured in getSurveyDetails for surveyId="+surveyId+"Language = "+languageName,e);
			  log.error("Error occured in getSurveyDetails for surveyId="+surveyId+"Language = "+languageName,e);
		  }
		return null;
	 }

	private String getTheTranslation(List<TransTranslationEntry> transTranslationEntries, int translationId, String languageCode) {
		
		for(TransTranslationEntry entry: transTranslationEntries)
		{
			if(translationId == entry.getId().getTranslationId() && entry.getId().getLanguageCode().equals(languageCode))
			{
				 return entry.getFieldText();
			}
		}
		return "";
	}



}
