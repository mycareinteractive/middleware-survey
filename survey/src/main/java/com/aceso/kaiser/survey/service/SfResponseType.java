package com.aceso.kaiser.survey.service;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import java.util.List;

public class SfResponseType implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String text;
	private List<SfNotificationType> notification;
	
	public SfResponseType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<SfNotificationType> getNotification() {
		return notification;
	}

	@XmlElementWrapper(name="notifications")
	@XmlElement(name="notification")
	public void setNotification(List<SfNotificationType> notification) {
		this.notification = notification;
	}


}