package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sf_question_answer_notification database table.
 * 
 */
@Entity
@Table(name="sf_question_answer_notification")
@NamedQuery(name="SfQuestionAnswerNotification.findAll", query="SELECT s FROM SfQuestionAnswerNotification s")
public class SfQuestionAnswerNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private byte notificationcondition;

	//bi-directional many-to-one association to SfNotification
	@ManyToOne
	@JoinColumn(name="notification_id")
	private SfNotification sfNotification;

	//bi-directional many-to-one association to SfQuestionAnswer
	@ManyToOne
	@JoinColumn(name="question_answer_id")
	private SfQuestionAnswer sfQuestionAnswer;

	public SfQuestionAnswerNotification() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getNotificationcondition() {
		return this.notificationcondition;
	}

	public void setNotificationcondition(byte notificationcondition) {
		this.notificationcondition = notificationcondition;
	}

	public SfNotification getSfNotification() {
		return this.sfNotification;
	}

	public void setSfNotification(SfNotification sfNotification) {
		this.sfNotification = sfNotification;
	}

	public SfQuestionAnswer getSfQuestionAnswer() {
		return this.sfQuestionAnswer;
	}

	public void setSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		this.sfQuestionAnswer = sfQuestionAnswer;
	}

}