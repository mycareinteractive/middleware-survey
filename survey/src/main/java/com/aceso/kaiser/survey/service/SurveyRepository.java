package com.aceso.kaiser.survey.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aceso.kaiser.survey.model.SfQuestion;
import com.aceso.kaiser.survey.model.SfSurvey;
import com.aceso.kaiser.survey.model.TransLanguage;

@Repository
public  class SurveyRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	protected static Logger log = LoggerFactory.getLogger("SurveyRepository");
	
	public List<SfSurvey> getSurveyDetails(String surveyId) {
		log.info("Entering getSurveyDetails in Repository class");
	
		Query query = sessionFactory.getCurrentSession().createQuery("from SfSurvey where id = ?");
		query.setParameter(0,new Integer(surveyId).intValue());
		return query.list();
		//Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SfQuestion.class);
		//criteria.add(Restrictions.eq("sfSurvey.id", new Integer(surveyId).intValue()));
		//return criteria.list();
				
	}
	
	
	public String getLanguageCode(String langName) {
		log.info("Entering getLanguageCode in Repository class");
	
		Query query = sessionFactory.getCurrentSession().createQuery("from TransLanguage where name = ?");
		query.setParameter(0,langName);
		TransLanguage transLanguage =  (TransLanguage) query.uniqueResult();
		 if(transLanguage != null)
			 return transLanguage.getCode();
		 else
			 return "en";  
	}
}
