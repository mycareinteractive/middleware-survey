package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sf_question_answer database table.
 * 
 */
@Entity
@Table(name="sf_question_answer")
@NamedQuery(name="SfQuestionAnswer.findAll", query="SELECT s FROM SfQuestionAnswer s")
public class SfQuestionAnswer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private int sequence;

	//bi-directional many-to-one association to SfAnswer
	@ManyToOne
	@JoinColumn(name="answer_id")
	private SfAnswer sfAnswer;

	//bi-directional many-to-one association to SfQuestion
	@ManyToOne
	@JoinColumn(name="question_id")
	private SfQuestion sfQuestion;

	//bi-directional many-to-one association to SfResponse
	@ManyToOne
	@JoinColumn(name="response_id")
	private SfResponse sfResponse;

	//bi-directional many-to-one association to SfQuestionAnswerNotification
	@OneToMany(mappedBy="sfQuestionAnswer")
	private List<SfQuestionAnswerNotification> sfQuestionAnswerNotifications;

	public SfQuestionAnswer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public SfAnswer getSfAnswer() {
		return this.sfAnswer;
	}

	public void setSfAnswer(SfAnswer sfAnswer) {
		this.sfAnswer = sfAnswer;
	}

	public SfQuestion getSfQuestion() {
		return this.sfQuestion;
	}

	public void setSfQuestion(SfQuestion sfQuestion) {
		this.sfQuestion = sfQuestion;
	}

	public SfResponse getSfResponse() {
		return this.sfResponse;
	}

	public void setSfResponse(SfResponse sfResponse) {
		this.sfResponse = sfResponse;
	}

	public List<SfQuestionAnswerNotification> getSfQuestionAnswerNotifications() {
		return this.sfQuestionAnswerNotifications;
	}

	public void setSfQuestionAnswerNotifications(List<SfQuestionAnswerNotification> sfQuestionAnswerNotifications) {
		this.sfQuestionAnswerNotifications = sfQuestionAnswerNotifications;
	}

	public SfQuestionAnswerNotification addSfQuestionAnswerNotification(SfQuestionAnswerNotification sfQuestionAnswerNotification) {
		getSfQuestionAnswerNotifications().add(sfQuestionAnswerNotification);
		sfQuestionAnswerNotification.setSfQuestionAnswer(this);

		return sfQuestionAnswerNotification;
	}

	public SfQuestionAnswerNotification removeSfQuestionAnswerNotification(SfQuestionAnswerNotification sfQuestionAnswerNotification) {
		getSfQuestionAnswerNotifications().remove(sfQuestionAnswerNotification);
		sfQuestionAnswerNotification.setSfQuestionAnswer(null);

		return sfQuestionAnswerNotification;
	}

}