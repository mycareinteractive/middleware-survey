package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sf_question database table.
 * 
 */
@Entity
@Table(name="sf_question")
@NamedQuery(name="SfQuestion.findAll", query="SELECT s FROM SfQuestion s")
public class SfQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String answertype;

	private String displaycondition;

	private int sequence;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="text")
	private TransTranslation transTranslation1;

	//bi-directional many-to-one association to SfSurvey
	@ManyToOne
	@JoinColumn(name="survey_id")
	private SfSurvey sfSurvey;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="title")
	private TransTranslation transTranslation2;

	//bi-directional many-to-one association to SfQuestionAnswer
	@OneToMany(mappedBy="sfQuestion")
	private List<SfQuestionAnswer> sfQuestionAnswers;

	public SfQuestion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnswertype() {
		return this.answertype;
	}

	public void setAnswertype(String answertype) {
		this.answertype = answertype;
	}

	public String getDisplaycondition() {
		return this.displaycondition;
	}

	public void setDisplaycondition(String displaycondition) {
		this.displaycondition = displaycondition;
	}

	public int getSequence() {
		return this.sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public TransTranslation getTransTranslation1() {
		return this.transTranslation1;
	}

	public void setTransTranslation1(TransTranslation transTranslation1) {
		this.transTranslation1 = transTranslation1;
	}

	public SfSurvey getSfSurvey() {
		return this.sfSurvey;
	}

	public void setSfSurvey(SfSurvey sfSurvey) {
		this.sfSurvey = sfSurvey;
	}

	public TransTranslation getTransTranslation2() {
		return this.transTranslation2;
	}

	public void setTransTranslation2(TransTranslation transTranslation2) {
		this.transTranslation2 = transTranslation2;
	}

	public List<SfQuestionAnswer> getSfQuestionAnswers() {
		return this.sfQuestionAnswers;
	}

	public void setSfQuestionAnswers(List<SfQuestionAnswer> sfQuestionAnswers) {
		this.sfQuestionAnswers = sfQuestionAnswers;
	}

	public SfQuestionAnswer addSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		getSfQuestionAnswers().add(sfQuestionAnswer);
		sfQuestionAnswer.setSfQuestion(this);

		return sfQuestionAnswer;
	}

	public SfQuestionAnswer removeSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		getSfQuestionAnswers().remove(sfQuestionAnswer);
		sfQuestionAnswer.setSfQuestion(null);

		return sfQuestionAnswer;
	}

}