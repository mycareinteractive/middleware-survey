package com.aceso.kaiser.survey.service;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


public class SfNotificationType implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String body;

	private String description;

	private String name;

	private String param;

	private String payload;

	private String subject;

	private String type;

	private String uri;
	
	private byte notificationcondition;

	public SfNotificationType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getPayload() {
		return this.payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUri() {
		return this.uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public byte getNotificationcondition() {
		return notificationcondition;
	}

	public void setNotificationcondition(byte notificationcondition) {
		this.notificationcondition = notificationcondition;
	}

	


}