package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the trans_translation database table.
 * 
 */
@Entity
@Table(name="trans_translation")
@NamedQuery(name="TransTranslation.findAll", query="SELECT t FROM TransTranslation t")
public class TransTranslation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	//bi-directional many-to-one association to SfAnswer
	@OneToMany(mappedBy="transTranslation")
	private List<SfAnswer> sfAnswers;

	//bi-directional many-to-one association to SfQuestion
	@OneToMany(mappedBy="transTranslation1")
	private List<SfQuestion> sfQuestions1;

	//bi-directional many-to-one association to SfQuestion
	@OneToMany(mappedBy="transTranslation2")
	private List<SfQuestion> sfQuestions2;

	//bi-directional many-to-one association to SfResponse
	@OneToMany(mappedBy="transTranslation")
	private List<SfResponse> sfResponses;

	//bi-directional many-to-one association to SfSurvey
	@OneToMany(mappedBy="transTranslation1")
	private List<SfSurvey> sfSurveys1;

	//bi-directional many-to-one association to SfSurvey
	@OneToMany(mappedBy="transTranslation2")
	private List<SfSurvey> sfSurveys2;

	//bi-directional many-to-one association to TransTranslationEntry
	@OneToMany(mappedBy="transTranslation")
	private List<TransTranslationEntry> transTranslationEntries;

	public TransTranslation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<SfAnswer> getSfAnswers() {
		return this.sfAnswers;
	}

	public void setSfAnswers(List<SfAnswer> sfAnswers) {
		this.sfAnswers = sfAnswers;
	}

	public SfAnswer addSfAnswer(SfAnswer sfAnswer) {
		getSfAnswers().add(sfAnswer);
		sfAnswer.setTransTranslation(this);

		return sfAnswer;
	}

	public SfAnswer removeSfAnswer(SfAnswer sfAnswer) {
		getSfAnswers().remove(sfAnswer);
		sfAnswer.setTransTranslation(null);

		return sfAnswer;
	}

	public List<SfQuestion> getSfQuestions1() {
		return this.sfQuestions1;
	}

	public void setSfQuestions1(List<SfQuestion> sfQuestions1) {
		this.sfQuestions1 = sfQuestions1;
	}

	public SfQuestion addSfQuestions1(SfQuestion sfQuestions1) {
		getSfQuestions1().add(sfQuestions1);
		sfQuestions1.setTransTranslation1(this);

		return sfQuestions1;
	}

	public SfQuestion removeSfQuestions1(SfQuestion sfQuestions1) {
		getSfQuestions1().remove(sfQuestions1);
		sfQuestions1.setTransTranslation1(null);

		return sfQuestions1;
	}

	public List<SfQuestion> getSfQuestions2() {
		return this.sfQuestions2;
	}

	public void setSfQuestions2(List<SfQuestion> sfQuestions2) {
		this.sfQuestions2 = sfQuestions2;
	}

	public SfQuestion addSfQuestions2(SfQuestion sfQuestions2) {
		getSfQuestions2().add(sfQuestions2);
		sfQuestions2.setTransTranslation2(this);

		return sfQuestions2;
	}

	public SfQuestion removeSfQuestions2(SfQuestion sfQuestions2) {
		getSfQuestions2().remove(sfQuestions2);
		sfQuestions2.setTransTranslation2(null);

		return sfQuestions2;
	}

	public List<SfResponse> getSfResponses() {
		return this.sfResponses;
	}

	public void setSfResponses(List<SfResponse> sfResponses) {
		this.sfResponses = sfResponses;
	}

	public SfResponse addSfRespons(SfResponse sfRespons) {
		getSfResponses().add(sfRespons);
		sfRespons.setTransTranslation(this);

		return sfRespons;
	}

	public SfResponse removeSfRespons(SfResponse sfRespons) {
		getSfResponses().remove(sfRespons);
		sfRespons.setTransTranslation(null);

		return sfRespons;
	}

	public List<SfSurvey> getSfSurveys1() {
		return this.sfSurveys1;
	}

	public void setSfSurveys1(List<SfSurvey> sfSurveys1) {
		this.sfSurveys1 = sfSurveys1;
	}

	public SfSurvey addSfSurveys1(SfSurvey sfSurveys1) {
		getSfSurveys1().add(sfSurveys1);
		sfSurveys1.setTransTranslation1(this);

		return sfSurveys1;
	}

	public SfSurvey removeSfSurveys1(SfSurvey sfSurveys1) {
		getSfSurveys1().remove(sfSurveys1);
		sfSurveys1.setTransTranslation1(null);

		return sfSurveys1;
	}

	public List<SfSurvey> getSfSurveys2() {
		return this.sfSurveys2;
	}

	public void setSfSurveys2(List<SfSurvey> sfSurveys2) {
		this.sfSurveys2 = sfSurveys2;
	}

	public SfSurvey addSfSurveys2(SfSurvey sfSurveys2) {
		getSfSurveys2().add(sfSurveys2);
		sfSurveys2.setTransTranslation2(this);

		return sfSurveys2;
	}

	public SfSurvey removeSfSurveys2(SfSurvey sfSurveys2) {
		getSfSurveys2().remove(sfSurveys2);
		sfSurveys2.setTransTranslation2(null);

		return sfSurveys2;
	}

	public List<TransTranslationEntry> getTransTranslationEntries() {
		return this.transTranslationEntries;
	}

	public void setTransTranslationEntries(List<TransTranslationEntry> transTranslationEntries) {
		this.transTranslationEntries = transTranslationEntries;
	}

	public TransTranslationEntry addTransTranslationEntry(TransTranslationEntry transTranslationEntry) {
		getTransTranslationEntries().add(transTranslationEntry);
		transTranslationEntry.setTransTranslation(this);

		return transTranslationEntry;
	}

	public TransTranslationEntry removeTransTranslationEntry(TransTranslationEntry transTranslationEntry) {
		getTransTranslationEntries().remove(transTranslationEntry);
		transTranslationEntry.setTransTranslation(null);

		return transTranslationEntry;
	}

}