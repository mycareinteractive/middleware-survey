package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sf_servicegroup database table.
 * 
 */
@Entity
@Table(name="sf_servicegroup")
@NamedQuery(name="SfServicegroup.findAll", query="SELECT s FROM SfServicegroup s")
public class SfServicegroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String roombed;

	private String servicegroup;

	public SfServicegroup() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoombed() {
		return this.roombed;
	}

	public void setRoombed(String roombed) {
		this.roombed = roombed;
	}

	public String getServicegroup() {
		return this.servicegroup;
	}

	public void setServicegroup(String servicegroup) {
		this.servicegroup = servicegroup;
	}

}