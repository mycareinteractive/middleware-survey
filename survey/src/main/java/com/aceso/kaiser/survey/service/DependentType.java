package com.aceso.kaiser.survey.service;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;


@XmlRootElement(name="dependent") 
public class DependentType implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String patientMRN;
	private String dependentMRN;
	private String relationship;

	public DependentType() {
	}

	public int getId() {
		return this.id;
	}

	public DependentType(int id, String patientMRN, String dependentMRN,String relationship) {
		super();
		this.id = id;
		this.setPatientMRN(patientMRN);
		this.setDependentMRN(dependentMRN);
		this.setRelationship(relationship);
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPatientMRN() {
		return patientMRN;
	}

	public void setPatientMRN(String patientMRN) {
		this.patientMRN = patientMRN;
	}

	public String getDependentMRN() {
		return dependentMRN;
	}

	public void setDependentMRN(String dependentMRN) {
		this.dependentMRN = dependentMRN;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	
	
}