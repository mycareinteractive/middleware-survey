package com.aceso.kaiser.survey.service;

import java.io.Serializable;

 


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;


@XmlRootElement(name="dependents") 
public class Dependents implements Serializable {
	private static final long serialVersionUID = 1L;
 
	private List<DependentType> dependents;

	public List<DependentType> getDependents() {
		return dependents;
	}
	
	@XmlElement(name="dependent")
	public void setDependents(List<DependentType> dependentList) {
		this.dependents = dependentList;
	}

}