package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sf_answer database table.
 * 
 */
@Entity
@Table(name="sf_answer")
@NamedQuery(name="SfAnswer.findAll", query="SELECT s FROM SfAnswer s")
public class SfAnswer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String value;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="text")
	private TransTranslation transTranslation;

	//bi-directional many-to-one association to SfQuestionAnswer
	@OneToMany(mappedBy="sfAnswer")
	private List<SfQuestionAnswer> sfQuestionAnswers;

	public SfAnswer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public TransTranslation getTransTranslation() {
		return this.transTranslation;
	}

	public void setTransTranslation(TransTranslation transTranslation) {
		this.transTranslation = transTranslation;
	}

	public List<SfQuestionAnswer> getSfQuestionAnswers() {
		return this.sfQuestionAnswers;
	}

	public void setSfQuestionAnswers(List<SfQuestionAnswer> sfQuestionAnswers) {
		this.sfQuestionAnswers = sfQuestionAnswers;
	}

	public SfQuestionAnswer addSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		getSfQuestionAnswers().add(sfQuestionAnswer);
		sfQuestionAnswer.setSfAnswer(this);

		return sfQuestionAnswer;
	}

	public SfQuestionAnswer removeSfQuestionAnswer(SfQuestionAnswer sfQuestionAnswer) {
		getSfQuestionAnswers().remove(sfQuestionAnswer);
		sfQuestionAnswer.setSfAnswer(null);

		return sfQuestionAnswer;
	}

}