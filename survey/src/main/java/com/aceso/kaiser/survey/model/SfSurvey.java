package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sf_survey database table.
 * 
 */
@Entity
@Table(name="sf_survey")
@NamedQuery(name="SfSurvey.findAll", query="SELECT s FROM SfSurvey s")
public class SfSurvey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private byte disabled;

	//bi-directional many-to-one association to SfQuestion
	@OneToMany(mappedBy="sfSurvey")
	private List<SfQuestion> sfQuestions;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="name")
	private TransTranslation transTranslation1;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="description")
	private TransTranslation transTranslation2;

	public SfSurvey() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getDisabled() {
		return this.disabled;
	}

	public void setDisabled(byte disabled) {
		this.disabled = disabled;
	}

	public List<SfQuestion> getSfQuestions() {
		return this.sfQuestions;
	}

	public void setSfQuestions(List<SfQuestion> sfQuestions) {
		this.sfQuestions = sfQuestions;
	}

	public SfQuestion addSfQuestion(SfQuestion sfQuestion) {
		getSfQuestions().add(sfQuestion);
		sfQuestion.setSfSurvey(this);

		return sfQuestion;
	}

	public SfQuestion removeSfQuestion(SfQuestion sfQuestion) {
		getSfQuestions().remove(sfQuestion);
		sfQuestion.setSfSurvey(null);

		return sfQuestion;
	}

	public TransTranslation getTransTranslation1() {
		return this.transTranslation1;
	}

	public void setTransTranslation1(TransTranslation transTranslation1) {
		this.transTranslation1 = transTranslation1;
	}

	public TransTranslation getTransTranslation2() {
		return this.transTranslation2;
	}

	public void setTransTranslation2(TransTranslation transTranslation2) {
		this.transTranslation2 = transTranslation2;
	}

}