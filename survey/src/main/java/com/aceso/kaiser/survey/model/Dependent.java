package com.aceso.kaiser.survey.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the dependent database table.
 * 
 */
@Entity
@Table(name="Dependent")
public class Dependent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String patientMRN;
	private String dependentMRN;
	private String relationship;

	@Id
	private int id;

  public Dependent() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPatientMRN() {
		return patientMRN;
	}

	public void setPatientMRN(String patientMRN) {
		this.patientMRN = patientMRN;
	}

	public String getDependentMRN() {
		return dependentMRN;
	}

	public void setDependentMRN(String dependentMRN) {
		this.dependentMRN = dependentMRN;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

}