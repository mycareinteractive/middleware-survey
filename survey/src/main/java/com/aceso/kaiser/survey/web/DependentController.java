package com.aceso.kaiser.survey.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.aceso.kaiser.survey.service.DependentType;
import com.aceso.kaiser.survey.service.DependentService;
import com.aceso.kaiser.survey.service.Dependents;
import com.aceso.kaiser.survey.service.EmailService;
import com.aceso.kaiser.survey.service.SfSurveyType;
import com.aceso.kaiser.survey.service.SurveyService;


@Controller
public class DependentController {

	@Autowired
	private DependentService dependentService;
	
	private static final Logger log = LoggerFactory.getLogger("DependentController");
	
	
		
	@RequestMapping("/getDependentXML")
	public void getDependentXML(@RequestParam("patientMRN") String patientMRN,HttpServletResponse response) {
    	try {
    		log.info("Entering DependentController : getDependentXML for patientMRN = "+patientMRN);
    		
    		Dependents dependents = dependentService.getDependentDetails(patientMRN);
    		 
   		    JAXBContext jc = JAXBContext.newInstance(Dependents.class);
    		Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        StringWriter st = new StringWriter(); 
	        marshaller.marshal(dependents , st); 
	        String xml = st.toString();
	        
	        log.info("Exiting DependentController : getDependentXML  = "+xml);
	        
    		PrintWriter printWriter = null;	        
	        printWriter = response.getWriter();
			printWriter.println(xml);
			printWriter.close(); 
			
		} catch (Exception e) {
			log.error("Error while retrieving getDependentXML" ,e);
		}
    }
	
}
