package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the trans_translation_entry database table.
 * 
 */
@Entity
@Table(name="trans_translation_entry")
@NamedQuery(name="TransTranslationEntry.findAll", query="SELECT t FROM TransTranslationEntry t")
public class TransTranslationEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TransTranslationEntryPK id;

	@Lob
	@Column(name="field_text")
	private String fieldText;

	//bi-directional many-to-one association to TransTranslation
	@ManyToOne
	@JoinColumn(name="translation_id",insertable=false, updatable=false)
	private TransTranslation transTranslation;

	public TransTranslationEntry() {
	}

	public TransTranslationEntryPK getId() {
		return this.id;
	}

	public void setId(TransTranslationEntryPK id) {
		this.id = id;
	}

	public String getFieldText() {
		return this.fieldText;
	}

	public void setFieldText(String fieldText) {
		this.fieldText = fieldText;
	}

	public TransTranslation getTransTranslation() {
		return this.transTranslation;
	}

	public void setTransTranslation(TransTranslation transTranslation) {
		this.transTranslation = transTranslation;
	}

}