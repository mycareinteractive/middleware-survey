package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the sf_surveyresult database table.
 * 
 */
@Entity
@Table(name="sf_surveyresult")
@NamedQuery(name="SfSurveyresult.findAll", query="SELECT s FROM SfSurveyresult s")
public class SfSurveyresult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="account_id")
	private int accountId;

	@Column(name="answer_id")
	private int answerId;

	@Column(name="answer_text")
	private String answerText;

	@Column(name="answer_value")
	private String answerValue;

	private String customanswertext;

	private Timestamp datestamp;

	@Column(name="notification_id_list")
	private String notificationIdList;

	@Column(name="notificationstatus_list")
	private String notificationstatusList;

	@Column(name="patientuser_id")
	private int patientuserId;

	@Column(name="question_id")
	private int questionId;

	@Column(name="question_text")
	private String questionText;

	private String roombed;

	@Column(name="survey_id")
	private int surveyId;

	@Column(name="survey_name")
	private String surveyName;

	public SfSurveyresult() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAccountId() {
		return this.accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getAnswerId() {
		return this.answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getAnswerText() {
		return this.answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public String getAnswerValue() {
		return this.answerValue;
	}

	public void setAnswerValue(String answerValue) {
		this.answerValue = answerValue;
	}

	public String getCustomanswertext() {
		return this.customanswertext;
	}

	public void setCustomanswertext(String customanswertext) {
		this.customanswertext = customanswertext;
	}

	public Timestamp getDatestamp() {
		return this.datestamp;
	}

	public void setDatestamp(Timestamp datestamp) {
		this.datestamp = datestamp;
	}

	public String getNotificationIdList() {
		return this.notificationIdList;
	}

	public void setNotificationIdList(String notificationIdList) {
		this.notificationIdList = notificationIdList;
	}

	public String getNotificationstatusList() {
		return this.notificationstatusList;
	}

	public void setNotificationstatusList(String notificationstatusList) {
		this.notificationstatusList = notificationstatusList;
	}

	public int getPatientuserId() {
		return this.patientuserId;
	}

	public void setPatientuserId(int patientuserId) {
		this.patientuserId = patientuserId;
	}

	public int getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestionText() {
		return this.questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getRoombed() {
		return this.roombed;
	}

	public void setRoombed(String roombed) {
		this.roombed = roombed;
	}

	public int getSurveyId() {
		return this.surveyId;
	}

	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}

	public String getSurveyName() {
		return this.surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

}