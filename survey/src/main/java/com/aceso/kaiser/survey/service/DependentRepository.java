package com.aceso.kaiser.survey.service;

import java.util.List;






import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aceso.kaiser.survey.model.Dependent;

@Repository
public  class DependentRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	protected static Logger log = LoggerFactory.getLogger("DependentRepository");
	
	public List<Dependent> getDependentDetails(String patientMRN) {
		log.info("Entering getDependentDetails in Repository class");
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Dependent where patientMRN = ?");
		query.setParameter(0,patientMRN);
		return query.list();
	}
}
