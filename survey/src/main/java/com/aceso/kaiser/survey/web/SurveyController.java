package com.aceso.kaiser.survey.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.aceso.kaiser.survey.service.EmailService;
import com.aceso.kaiser.survey.service.SfSurveyType;
import com.aceso.kaiser.survey.service.SurveyService;


@Controller
public class SurveyController {

	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private EmailService emailService;

	private static final Logger log = LoggerFactory.getLogger("SurveyController");
	
	@RequestMapping("/sendEmail.htm")
	public @ResponseBody String sendEmail()
	{
		log.info("Entering SurveyController : sendEmail");
		emailService.sendMail("sirishareddy226@gmail.com", "sirishareddy226@gmail.com", "TestSubject", "Test Msg");
		return "success";
	}
	
		
	@RequestMapping("/getSurveyXML")
	public void getSurveyXML(@RequestParam("surveyId") String surveyId,@RequestParam("lang") String lang,HttpServletResponse response) {
    	try {
    		log.info("Entering SurveyController : getSurveyXML for surveyId = "+surveyId+"Language = "+lang);
    		
    		SfSurveyType surveyType = surveyService.generateSurveyXML(surveyId,lang);
    		 
    		 
    		JAXBContext jc = JAXBContext.newInstance(SfSurveyType.class);
    		Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        StringWriter st = new StringWriter(); 
	        marshaller.marshal(surveyType , st); 
	        String xml = st.toString();
	        
	        log.info("Exiting SurveyController : getSurveyXML  = "+xml);
	        
    		PrintWriter printWriter = null;	        
	        printWriter = response.getWriter();
			printWriter.println(xml);
			printWriter.close();
			
			
		} catch (Exception e) {
			log.error("Error while retrieving surveyXml" ,e);
		}
    }
	
}
