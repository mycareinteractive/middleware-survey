package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the sf_notification database table.
 * 
 */
@Entity
@Table(name="sf_notification")
@NamedQuery(name="SfNotification.findAll", query="SELECT s FROM SfNotification s")
public class SfNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String body;

	private String description;

	private String name;

	private String param;

	private String payload;

	private String subject;

	private String type;

	private String uri;

	//bi-directional many-to-one association to SfQuestionAnswerNotification
	@OneToMany(mappedBy="sfNotification")
	private List<SfQuestionAnswerNotification> sfQuestionAnswerNotifications;

	public SfNotification() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getPayload() {
		return this.payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUri() {
		return this.uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<SfQuestionAnswerNotification> getSfQuestionAnswerNotifications() {
		return this.sfQuestionAnswerNotifications;
	}

	public void setSfQuestionAnswerNotifications(List<SfQuestionAnswerNotification> sfQuestionAnswerNotifications) {
		this.sfQuestionAnswerNotifications = sfQuestionAnswerNotifications;
	}

	public SfQuestionAnswerNotification addSfQuestionAnswerNotification(SfQuestionAnswerNotification sfQuestionAnswerNotification) {
		getSfQuestionAnswerNotifications().add(sfQuestionAnswerNotification);
		sfQuestionAnswerNotification.setSfNotification(this);

		return sfQuestionAnswerNotification;
	}

	public SfQuestionAnswerNotification removeSfQuestionAnswerNotification(SfQuestionAnswerNotification sfQuestionAnswerNotification) {
		getSfQuestionAnswerNotifications().remove(sfQuestionAnswerNotification);
		sfQuestionAnswerNotification.setSfNotification(null);

		return sfQuestionAnswerNotification;
	}

}