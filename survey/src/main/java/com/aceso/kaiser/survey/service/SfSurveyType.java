package com.aceso.kaiser.survey.service;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;


@XmlRootElement(name="survey") 
public class SfSurveyType implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private int id;
	private String desc;
	private byte disabled;
	private String name;
	private List<SfQuestionType> question;

	public SfSurveyType() {
	}

	public int getId() {
		return this.id;
	}

	public SfSurveyType(int id, String desc, byte disabled, String name) {
		super();
		this.id = id;
		this.desc = desc;
		this.disabled = disabled;
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public byte getDisabled() {
		return this.disabled;
	}

	public void setDisabled(byte disabled) {
		this.disabled = disabled;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

	public List<SfQuestionType> getQuestions() {
		return question;
	}

	@XmlElementWrapper(name="questions")
	@XmlElement(name="question")
	public void setQuestions(List<SfQuestionType> sfQuestionTypes) {
		this.question = sfQuestionTypes;
	}

	@Override
	public String toString() {
		return "SfSurveyType [id=" + id + ", desc=" + desc + ", disabled="
				+ disabled + ", name=" + name + "]";
	}

	
}