package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the trans_language database table.
 * 
 */
@Entity
@Table(name="trans_language")
@NamedQuery(name="TransLanguage.findAll", query="SELECT t FROM TransLanguage t")
public class TransLanguage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private String name;

	public TransLanguage() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}