package com.aceso.kaiser.survey.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the locale database table.
 * 
 */
@Entity
@NamedQuery(name="Locale.findAll", query="SELECT l FROM Locale l")
public class Locale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String desc;

	public Locale() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}